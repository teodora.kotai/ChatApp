Pasii de compilare, instalare si lansare a aplicatiei:
- descarcarea si instalarea mediului de dezvoltare Android Studio
- crearea unui proiect nou
- pentru compilare, se selecteaza Build apoi Make Project
- pentru a rula pe un emulator se creaza un Device, se alege Run si apoi Run app
- pentru instalarea aplicatiei pe un telefon se va alege Build, Build Bundle(s)/APK(s), Build APK(s)
- se localizeaza fisierul APK in folder si se trimite pe telefonul dorit, unde se instaleaza manual si pe urma se lanseaza aplicatia

Adresa repository-ului: https://github.com/teokotai/ChatApp.git
